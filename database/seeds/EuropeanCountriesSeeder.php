<?php

use Brewmap\Models\Country;
use Illuminate\Database\Seeder;

class EuropeanCountriesSeeder extends Seeder {

	const EUROPEAN_COUNTRIES = [
		["name" => "Albania", "symbol" => "al"],
		["name" => "Andora", "symbol" => "ad"],
		["name" => "Austria", "symbol" => "at"],
		["name" => "Belgia", "symbol" => "be"],
		["name" => "Białoruś", "symbol" => "by"],
		["name" => "Bośnia i Hercegowina", "symbol" => "ba"],
		["name" => "Bułgaria", "symbol" => "bg"],
		["name" => "Chorwacja", "symbol" => "hr"],
		["name" => "Czarnogóra", "symbol" => "me"],
		["name" => "Cypr", "symbol" => "cy"],
		["name" => "Czechy", "symbol" => "cz"],
		["name" => "Dania", "symbol" => "dk"],
		["name" => "Estonia", "symbol" => "ee"],
		["name" => "Finlandia", "symbol" => "fi"],
		["name" => "Francja", "symbol" => "fr"],
		["name" => "Grecja", "symbol" => "gr"],
		["name" => "Hiszpania", "symbol" => "es"],
		["name" => "Holandia", "symbol" => "nl"],
		["name" => "Irlandia", "symbol" => "ie"],
		["name" => "Islandia", "symbol" => "is"],
		["name" => "Kazachstan", "symbol" => "kz"],
		["name" => "Liechtenstein", "symbol" => "li"],
		["name" => "Litwa", "symbol" => "lt"],
		["name" => "Luksemburg", "symbol" => "lu"],
		["name" => "Łotwa", "symbol" => "lv"],
		["name" => "Macedonia", "symbol" => "mk"],
		["name" => "Malta", "symbol" => "mt"],
		["name" => "Mołdawia", "symbol" => "md"],
		["name" => "Monako", "symbol" => "mc"],
		["name" => "Niemcy", "symbol" => "de"],
		["name" => "Norwegia", "symbol" => "no"],
		["name" => "Polska", "symbol" => "pl"],
		["name" => "Portugalia", "symbol" => "pt"],
		["name" => "Rosja", "symbol" => "ru"],
		["name" => "Rumunia", "symbol" => "ro"],
		["name" => "San Marino", "symbol" => "sm"],
		["name" => "Serbia", "symbol" => "rs"],
		["name" => "Słowacja", "symbol" => "sk"],
		["name" => "Słowenia", "symbol" => "si"],
		["name" => "Szwajcaria", "symbol" => "ch"],
		["name" => "Szwecja", "symbol" => "se"],
		["name" => "Turcja", "symbol" => "tr"],
		["name" => "Ukraina", "symbol" => "ua"],
		["name" => "Watykan", "symbol" => "va"],
		["name" => "Węgry", "symbol" => "hu"],
		["name" => "Wielka Brytania", "symbol" => "gb"],
		["name" => "Włochy", "symbol" => "it"],
	];

	public function run(): void {
		foreach(self::EUROPEAN_COUNTRIES as $country) {
			Country::firstOrCreate($country);
		}
	}

}
