<?php

use Brewmap\Models\Brewery;
use Brewmap\Models\Map;
use Brewmap\Models\MapBrewery;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

	public function run() {
		$this->call(EuropeanCountriesSeeder::class);

		if(app()->environment("local", "testing")) {
			factory(Brewery::class, 250)->create();

			$breweries = Brewery::pluck("uuid");
			factory(Map::class, 10)->create()->each(function(Map $map) use($breweries) {
				foreach(range(1, rand(2, 5)) as $n) {
					MapBrewery::create([
						"map_uuid" => $map->uuid,
						"brewery_uuid" => $breweries->random(),
					]);
				}
			});
		}
	}

}
