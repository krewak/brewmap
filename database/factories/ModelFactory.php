<?php

use Brewmap\Models\Brewery;
use Brewmap\Models\BreweryStatus;
use Brewmap\Models\Country;
use Brewmap\Models\Map;
use Brewmap\Models\User;
use Faker\Generator;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(User::class, function(Generator $faker) {
	return [
		"name" => $faker->name,
		"email" => $faker->email,
	];
});

$countriesUuids = Country::pluck("uuid");

$factory->define(Brewery::class, function(Generator $faker) use($countriesUuids) {
	$name = $faker->company;

	return [
		"name" => $name,
		"alt_name" => $faker->optional(0.05, "")->company,
		"search_name" => $name,
		"latitude" => $faker->latitude(49.002380, 54.833333),
		"longitude" => $faker->longitude(14.122980, 22.847100),
		"country_uuid" => $countriesUuids->random(),
		"status_id" => $faker->randomElement(array_keys(BreweryStatus::STATUSES)),
	];
});


$factory->define(Map::class, function(Generator $faker) {
	return [
		"name" => $faker->company,
		"description" => $faker->text,
		"cover_url" => $faker->imageUrl(640, 480, "city")
	];
});
