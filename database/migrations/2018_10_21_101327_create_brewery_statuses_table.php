<?php

use Brewmap\Models\BreweryStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBreweryStatusesTable extends Migration {

	public function up(): void {
		Schema::create("brewery_statuses", function(Blueprint $table) {
			$table->string("id")->unique();
			$table->string("name");
			$table->timestamps();
		});

		foreach(BreweryStatus::STATUSES as $id => $name) {
			BreweryStatus::create(["id" => $id, "name" => $name]);
		}
	}

	public function down(): void {
		Schema::dropIfExists("brewery_statuses");
	}
}
