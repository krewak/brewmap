<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapsTable extends Migration {

	public function up(): void {
		Schema::create("maps", function(Blueprint $table) {
			$table->string("uuid")->unique();
			$table->string("slug")->unique();

			$table->string("name");
			$table->text("description")->default("");
			$table->string("cover_url")->default("");
			$table->integer("breweries_count")->default(0);

			$table->string("parent_map_uuid")->nullable()->default(null);

			$table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
			$table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));

		});

		Schema::table("maps", function(Blueprint $table) {
			$table->foreign("parent_map_uuid")->references("uuid")->on("maps");
		});
	}

	public function down(): void {
		Schema::table("maps", function(Blueprint $table) {
			$table->dropForeign(["parent_map_uuid"]);
		});

		Schema::dropIfExists("maps");
	}
}
