<?php

use Brewmap\Models\BreweryStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBreweriesTable extends Migration {

	public function up(): void {
		Schema::create("breweries", function(Blueprint $table) {
			$table->string("uuid")->unique();
			$table->string("slug")->unique();

			$table->string("name");
			$table->string("alt_name")->default("");
			$table->string("full_name")->default("");
			$table->string("search_name")->default("");

			$table->decimal("latitude", 10, 6);
			$table->decimal("longitude", 10, 6);

			$table->string("country_uuid");
			$table->foreign("country_uuid")->references("uuid")->on("countries");

			$table->string("status_id")->default(BreweryStatus::getDefaultStatus());
			$table->foreign("status_id")->references("id")->on("brewery_statuses");

			$table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
			$table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));

		});
	}

	public function down(): void {
		Schema::dropIfExists("breweries");
	}
}
