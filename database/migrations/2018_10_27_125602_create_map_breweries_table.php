<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapBreweriesTable extends Migration {

	public function up(): void {
		Schema::create("map_breweries", function(Blueprint $table) {
			$table->string("map_uuid");
			$table->foreign("map_uuid")->references("uuid")->on("maps");
			$table->string("brewery_uuid");
			$table->foreign("brewery_uuid")->references("uuid")->on("breweries");

			$table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
			$table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));

		});
	}

	public function down(): void {
		Schema::dropIfExists("map_breweries");
	}
}
