<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration {

	public function up(): void {
		Schema::create("countries", function(Blueprint $table) {
			$table->string("uuid")->unique();
			$table->string("slug")->unique();

			$table->string("name");
			$table->string("symbol");

			$table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
			$table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));

		});
	}

	public function down(): void {
		Schema::dropIfExists("countries");
	}
}
