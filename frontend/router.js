import Vue from "vue"
import Router from "vue-router"

import Base from "./Base"
import Home from "./views/Home"
import Brewery from "./views/Brewery"
import Breweries from "./views/Breweries"
import Log from "./views/Log"
import Map from "./views/Map"
import Maps from "./views/Maps"
import Search from "./views/Search"
import Settings from "./views/Settings"

import Dashboard from "./Dashboard"
import DashboardView from "./dashboard/Dashboard"

Vue.use(Router)

const routes = [
	{ path: "/", redirect: { name: "home" }, component: Base, children: [
		{ path: "mapa", name: "home", component: Home, meta: { section: "home" } },
		{ path: "mapy", name: "maps", component: Maps, meta: { section: "maps" } },
		{ path: "mapa/:slug", name: "map", component: Map, meta: { section: "maps" } },
		{ path: "browary", name: "breweries", component: Breweries, meta: { section: "breweries" } },
		{ path: "browar/:slug", name: "brewery", component: Brewery, meta: { section: "breweries" } },
		{ path: "szukaj", name: "search", component: Search, meta: { section: "search" } },
		{ path: "ustawienia", name: "settings", component: Settings, meta: { section: "settings" } },
		{ path: "logi", name: "log", component: Log, meta: { section: "log" } },
	] },
	{ path: "/dashboard", redirect: { name: "dashboard" }, component: Dashboard, children: [
		{ path: "dashboard", name: "dashboard", component: DashboardView },
	] },
]

export default new Router({
	mode: "history",
	routes: routes
})
