import Vue from "vue"
import axios from "axios"
import VueCookies from "vue-cookies"
import App from "./App.vue"

import router from "./router"
import store from "./store"
import "semantic-ui-css/semantic.min.css"

Vue.prototype.$http = axios
axios.defaults.baseURL = "/api/"

Vue.use(VueCookies)

let application = new Vue({
	router,
	store,
	render: h => h(App)
})

application.$mount("#app")
