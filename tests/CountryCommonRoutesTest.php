<?php

namespace Brewmap\Tests;

use Brewmap\Models\Country;
use Illuminate\Http\Response;
use Laravel\Lumen\Testing\DatabaseMigrations;

class CountryCommonRoutesTest extends TestCase {

	use DatabaseMigrations;

	public function testEmptyCountriesCollectionResponse(): void {
		$this->get("/api/countries");
		$this->assertResponseStatus(Response::HTTP_OK);
		$this->assertEmpty($this->getResponseData()->data);
	}

	public function testCountriesCollectionResponse(): void {
		Country::createEntryForTesting();

		$this->get("/api/countries");
		$this->assertResponseStatus(Response::HTTP_OK);
		$this->assertCount(1, $this->getResponseData()->data);
	}

}
