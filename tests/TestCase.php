<?php

namespace Brewmap\Tests;

use Laravel\Lumen\Testing\TestCase as BaseTestCase;
use stdClass;

abstract class TestCase extends BaseTestCase {

	public function createApplication() {
		return require __DIR__ . '/../app/bootstrap.php';
	}

	public function getResponseData(): stdClass {
		return json_decode($this->response->content());
	}

}
