<?php

namespace Brewmap\Tests;

use Brewmap\Models\Brewery;
use Brewmap\Models\Country;
use Laravel\Lumen\Testing\DatabaseMigrations;

class BreweryCreatingTest extends TestCase {

	use DatabaseMigrations;

	public function setUp() {
		parent::setUp();
		Country::createEntryForTesting();
	}

	public function testCreatingBrewerySlug(): void {
		$brewery = factory(Brewery::class, 1)->create([
			"name" => "Brewery",
		])->first();

		$this->assertEquals("brewery", $brewery->slug);

		$brewery = factory(Brewery::class, 1)->create([
			"name" => "Brewery X",
		])->first();

		$this->assertEquals("brewery-x", $brewery->slug);

		$brewery = factory(Brewery::class, 1)->create([
			"name" => "Brewery",
		])->first();

		$this->assertEquals("brewery-2", $brewery->slug);

		$brewery = factory(Brewery::class, 1)->create([
			"name" => "Brewery X",
		])->first();

		$this->assertEquals("brewery-x-2", $brewery->slug);
	}

	public function testCreatingPolishBrewerySlug(): void {
		$brewery = factory(Brewery::class, 1)->create([
			"name" => "Browar Żółć",
		])->first();

		$this->assertEquals("browar-zolc", $brewery->slug);
	}

	public function testCreatingGermanBrewerySlug(): void {
		$brewery = factory(Brewery::class, 1)->create([
			"name" => "Größenmaßstäbe Braurei",
		])->first();

		$this->assertEquals("grossenmassstabe-braurei", $brewery->slug);
	}

}
