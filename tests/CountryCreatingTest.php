<?php

namespace Brewmap\Tests;

use Brewmap\Models\Country;
use Laravel\Lumen\Testing\DatabaseMigrations;

class CountryCreatingTest extends TestCase {

	use DatabaseMigrations;

	public function testCreatingCountrySlug(): void {
		$country = Country::create(["name" => "Sokovia", "symbol" => "so"]);
		$this->assertEquals("sokovia", $country->slug);

		$country = Country::create(["name" => "Sokovia", "symbol" => "so"]);
		$this->assertEquals("sokovia-2", $country->slug);
	}

	public function testCreatingPolishCountrySlug(): void {
		$country = Country::create(["name" => "Bośnia i Hercegowina", "symbol" => "ba"]);
		$this->assertEquals("bosnia-i-hercegowina", $country->slug);
	}

}
