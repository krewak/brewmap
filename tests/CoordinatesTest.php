<?php

namespace Brewmap\Tests;

use Brewmap\Exceptions\InvalidCoordinateValue;
use Brewmap\Helpers\Coordinates;

class CoordinatesTest extends TestCase {

	/**
	 * @throws InvalidCoordinateValue
	 */
	public function testProperCoordinates() {
		$latitude = 51.212250;
		$longitude = 16.168097;
		$coordinates = new Coordinates($latitude, $longitude);

		$this->assertEquals($coordinates->getLatitude(), $latitude);
		$this->assertEquals($this->getMantissaLength($this->getMantissa($coordinates->getLatitude())), Coordinates::MANTISSA);

		$this->assertEquals($coordinates->getLongitude(), $longitude);
		$this->assertEquals($this->getMantissaLength($this->getMantissa($coordinates->getLongitude())), Coordinates::MANTISSA);
	}

	/**
	 * @throws InvalidCoordinateValue
	 */
	public function testImproperCoordinates() {
		$latitude = 1000;
		$longitude = 1000;

		$this->expectException(InvalidCoordinateValue::class);
		new Coordinates($latitude, $longitude);
	}

	private function getMantissaLength(string $mantissa): int {
		return strlen($mantissa);
	}

	private function getMantissa(string $coordinate): string {
		return explode(".", $coordinate)[1];
	}
}
