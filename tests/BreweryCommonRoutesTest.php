<?php

namespace Brewmap\Tests;

use Brewmap\Http\Controllers\BreweriesController;
use Brewmap\Models\Brewery;
use Brewmap\Models\BreweryStatus;
use Brewmap\Models\Country;
use Illuminate\Http\Response;
use Laravel\Lumen\Testing\DatabaseMigrations;

class BreweryCommonRoutesTest extends TestCase {

	use DatabaseMigrations;

	public function setUp() {
		parent::setUp();
		Country::createEntryForTesting();
	}

	public function testSingleNonExistingBreweryResponse(): void {
		$this->get("/api/brewery/non-existing-brewery");
		$this->assertResponseStatus(Response::HTTP_NOT_FOUND);
		$this->assertEmpty($this->getResponseData()->data);
	}

	public function testSingleExistingBreweryResponse(): void {
		$brewery = factory(Brewery::class, 1)->create([
			"status_id" => BreweryStatus::BREWERY
		])->first();

		$this->get("/api/brewery/" . $brewery->slug);
		$this->assertResponseStatus(Response::HTTP_OK);
		$this->assertNotEmpty($this->getResponseData()->data);
	}

	public function testSingleExistingDraftBreweryResponse(): void {
		$brewery = factory(Brewery::class, 1)->create([
			"status_id" => BreweryStatus::DRAFT
		])->first();

		$this->get("/api/brewery/" . $brewery->slug);
		$this->assertResponseStatus(Response::HTTP_NOT_FOUND);
		$this->assertEmpty($this->getResponseData()->data);
	}

	public function testEmptyBreweriesCollectionResponse(): void {
		$this->get("/api/breweries");
		$this->assertResponseStatus(Response::HTTP_OK);
		$this->assertEmpty($this->getResponseData()->data);
	}

	public function testBreweriesCollectionResponse(): void {
		$breweriesCount = 10;

		factory(Brewery::class, $breweriesCount)->create([
			"status_id" => BreweryStatus::BREWERY
		]);

		$this->get("/api/breweries");
		$this->assertResponseStatus(Response::HTTP_OK);
		$this->assertCount($breweriesCount, $this->getResponseData()->data);
	}

	public function testHugeBreweriesCollectionResponse(): void {
		$expectedBreweriesCount = BreweriesController::STANDARD_COLLECTION_LIMIT;
		$breweriesCount = $expectedBreweriesCount + 1;

		factory(Brewery::class, $breweriesCount)->create([
			"status_id" => BreweryStatus::BREWERY
		]);

		$this->get("/api/breweries");
		$this->assertResponseStatus(Response::HTTP_OK);
		$this->assertCount($expectedBreweriesCount, $this->getResponseData()->data);
	}

	public function testAreaLimitedBreweriesCollectionResponse(): void {
		$breweriesCount = 3;
		$northLimit = 10;
		$eastLimit = 10;
		$southLimit = 0;
		$westLimit = 0;

		factory(Brewery::class, $breweriesCount)->create([
			"status_id" => BreweryStatus::BREWERY,
			"latitude" => rand($southLimit, $northLimit),
			"longitude" => rand($westLimit, $eastLimit),
		]);

		factory(Brewery::class, $breweriesCount)->create([
			"status_id" => BreweryStatus::DRAFT,
			"latitude" => rand($southLimit, $northLimit),
			"longitude" => rand($westLimit, $eastLimit),
		]);

		factory(Brewery::class, $breweriesCount)->create([
			"status_id" => BreweryStatus::BREWERY,
			"latitude" => rand(2 * $northLimit, 3 * $northLimit),
			"longitude" => rand(2 * $eastLimit, 2 * $eastLimit),
		]);

		$this->get("/api/breweries/$northLimit/$eastLimit/$southLimit/$westLimit");
		$this->assertResponseStatus(Response::HTTP_OK);
		$this->assertCount($breweriesCount, $this->getResponseData()->data);

		foreach($this->getResponseData()->data as $brewery) {
			$this->assertLessThanOrEqual($northLimit, $brewery->coordinates->latitude);
			$this->assertGreaterThanOrEqual($southLimit, $brewery->coordinates->latitude);
			$this->assertLessThanOrEqual($eastLimit, $brewery->coordinates->longitude);
			$this->assertGreaterThanOrEqual($westLimit, $brewery->coordinates->longitude);
		}
	}

	public function testAreaLimitedEmptyBreweriesCollectionResponse(): void {
		$breweriesCount = 3;
		$expectedBreweriesCount = 0;
		$northLimit = 10;
		$eastLimit = 10;
		$southLimit = 0;
		$westLimit = 0;

		factory(Brewery::class, $breweriesCount)->create([
			"status_id" => BreweryStatus::BREWERY,
			"latitude" => rand(2 * $northLimit, 3 * $northLimit),
			"longitude" => rand(2 * $eastLimit, 2 * $eastLimit),
		]);

		$this->get("/api/breweries/$northLimit/$eastLimit/$southLimit/$westLimit");
		$this->assertResponseStatus(Response::HTTP_OK);
		$this->assertCount($expectedBreweriesCount, $this->getResponseData()->data);
	}

}
