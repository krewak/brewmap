<?php

namespace Brewmap\Tests;

use Brewmap\Http\Controllers\ApplicationController;
use Illuminate\Http\Response;

class ApplicationStatusTest extends TestCase {

	public function testApplicationStatus() {
		$this->get("/api/");
		$this->assertResponseStatus(Response::HTTP_OK);
		$this->assertEquals(ApplicationController::STATUS_MESSAGE, $this->getResponseData()->messages[0]);
	}
}
