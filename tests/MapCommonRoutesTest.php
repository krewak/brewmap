<?php

namespace Brewmap\Tests;

use Brewmap\Models\Map;
use Illuminate\Http\Response;
use Laravel\Lumen\Testing\DatabaseMigrations;

class MapCommonRoutesTest extends TestCase {

	use DatabaseMigrations;

	public function testSingleNonExistingMapResponse(): void {
		$this->get("/api/map/non-existing-map");
		$this->assertResponseStatus(Response::HTTP_NOT_FOUND);
		$this->assertEmpty($this->getResponseData()->data);
	}

	public function testSingleExistingMapResponse(): void {
		$map = Map::createEntryForTesting();

		$this->get("/api/map/" . $map->slug);
		$this->assertResponseStatus(Response::HTTP_OK);
		$this->assertNotEmpty($this->getResponseData()->data);
	}

	public function testEmptyMapsCollectionResponse(): void {
		$this->get("/api/maps");
		$this->assertResponseStatus(Response::HTTP_OK);
		$this->assertEmpty($this->getResponseData()->data);
	}

	public function testMapsCollectionResponse(): void {
		Map::createEntryForTesting();

		$this->get("/api/maps");
		$this->assertResponseStatus(Response::HTTP_OK);
		$this->assertCount(1, $this->getResponseData()->data);
	}

}
