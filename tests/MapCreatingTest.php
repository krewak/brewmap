<?php

namespace Brewmap\Tests;

use Brewmap\Models\Map;
use Laravel\Lumen\Testing\DatabaseMigrations;

class MapCreatingTest extends TestCase {

	use DatabaseMigrations;

	public function testCreatingMapSlug(): void {
		$map = Map::create(["name" => "Polskie browary"]);
		$this->assertEquals("polskie-browary", $map->slug);

		$map = Map::create(["name" => "Polskie browary"]);
		$this->assertEquals("polskie-browary-2", $map->slug);
	}

	public function testCreatingPolishMapSlug(): void {
		$map = Map::create(["name" => "Dolnośląskie"]);
		$this->assertEquals("dolnoslaskie", $map->slug);
	}

}
