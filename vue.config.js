const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = {
	css: {
		sourceMap: true,
	},
	outputDir: "public",
	configureWebpack: {
		resolve: {
			alias: {
				"@": "./frontend"
			}
		},
		entry: {
			app: "./frontend/main.js"
		},
		plugins: [
			new HtmlWebpackPlugin({
				template: "./frontend/templates/index.html"
			})
		]
	},
	devServer: {
		port: 43751,
		proxy: {
			"/api": {
				target: "http://brewmap.local/"
			},
		}
	},
}
