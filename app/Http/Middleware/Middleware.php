<?php

namespace Brewmap\Http\Middleware;

use Brewmap\Interfaces\IsSystemResource;
use Closure;
use Illuminate\Http\Request;

abstract class Middleware {

	/**
	 * @var IsSystemResource
	 */
	protected $resource;

	public function __construct(IsSystemResource $resource) {
		$this->resource = $resource;
	}

	abstract public function handle(Request $request, Closure $next, $guard = null);

}
