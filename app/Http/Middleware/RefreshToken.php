<?php

namespace Brewmap\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Brewmap\Services\JSONWebTokenService;

class RefreshToken extends Middleware {

	public function handle(Request $request, Closure $next, $guard = null) {
		if($request["token"]) {
			$service = app(JSONWebTokenService::class);
			$request["token"] = $service->extendTokenExpirationDate($request["token"]);
		}

		return $next($request);
	}

}
