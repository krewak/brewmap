<?php

namespace Brewmap\Http\Middleware;

use Closure;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Authenticated extends Middleware {

	public function handle(Request $request, Closure $next, $guard = null) {
		if(!$request["token"]) {
			return $this->resource
				->pushMessage("Token not provided.")
				->response()
				->setStatusCode(Response::HTTP_UNAUTHORIZED);
		}

		if($request["exception"]) {
			if($request["exception"] instanceof ExpiredException) {
				return $this->resource
					->pushMessage("Provided token is expired.")
					->response()
					->setStatusCode(Response::HTTP_BAD_REQUEST);
			}

			return $this->resource
				->pushMessage("An error while decoding token.")
				->response()
				->setStatusCode(Response::HTTP_BAD_REQUEST);
		}

		return $next($request);
	}

}
