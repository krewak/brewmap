<?php

namespace Brewmap\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Guest extends Middleware {

	public function handle(Request $request, Closure $next, $guard = null) {
		if($request["token"] && !$request["exception"]) {
			return $this->resource
				->pushMessage("Area restricted for guests.")
				->response()
				->setStatusCode(Response::HTTP_UNAUTHORIZED);
		}

		return $next($request);
	}

}
