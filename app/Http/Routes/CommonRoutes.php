<?php

namespace Brewmap\Http\Routes;

use Laravel\Lumen\Routing\Router;

class CommonRoutes extends Routes {

	public function __construct(Router $router) {
		$router->get("/", ["as" => "status", "uses" => "ApplicationController@getStatus"]);

		$router->get("/map/default/bounds", ["as" => "map.default.bounds", "uses" => "DefaultMapSettingsController@getDefaultBounds"]);

		$router->get("/breweries/", ["as" => "breweries", "uses" => "BreweriesController@getAll"]);
		$router->get("/breweries/{map}/{north}/{east}/{south}/{west}", ["as" => "breweries.map.limited", "uses" => "BreweriesController@getAllInLimits"]);
		$router->get("/breweries/{north}/{east}/{south}/{west}", ["as" => "breweries.limited", "uses" => "BreweriesController@getAllInLimits"]);
		$router->get("/breweries/search/{query}", ["as" => "breweries.search", "uses" => "BreweriesController@getBySearchPhrase"]);
		$router->get("/breweries/random", ["as" => "breweries.random", "uses" => "BreweryController@getRandom"]);
		$router->get("/brewery/{slug}", ["as" => "brewery", "uses" => "BreweryController@getBySlug"]);

		$router->get("/countries/", ["as" => "countries", "uses" => "CountriesController@getAll"]);

		$router->get("/maps/", ["as" => "maps", "uses" => "MapsController@getAll"]);
		$router->get("/map/{slug}", ["as" => "map", "uses" => "MapController@getBySlug"]);
	}

}
