<?php

namespace Brewmap\Http\Routes;

use Laravel\Lumen\Routing\Router;
use Brewmap\Interfaces\DispatchesRoutes;

abstract class Routes implements DispatchesRoutes {

	abstract public function __construct(Router $router);

}

