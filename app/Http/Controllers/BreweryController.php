<?php

namespace Brewmap\Http\Controllers;

use Brewmap\Exceptions\BreweryNotFound;
use Brewmap\Models\Brewery;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

class BreweryController extends Controller {

	const SINGLE_COLLECTION_LIMIT = 1;

	/**
	 * @param string $slug
	 * @return JsonResponse
	 * @throws BreweryNotFound
	 */
	public function getBySlug(string $slug): JsonResponse {
		try {
			$brewery = Brewery::query()
				->where("slug", $slug)
				->firstOrFail();
		} catch(ModelNotFoundException $exception) {
			throw new BreweryNotFound();
		}

		return $this->resource
			->resource($brewery)
			->response();
	}

	/**
	 * @return JsonResponse
	 */
	public function getRandom(): JsonResponse {
		$brewery = Brewery::query()
			->orderByRaw("RAND()")
			->limit(self::SINGLE_COLLECTION_LIMIT)
			->first();

		return $this->resource
			->resource($brewery)
			->response();
	}

}
