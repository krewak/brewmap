<?php

namespace Brewmap\Http\Controllers;

use Illuminate\Http\JsonResponse;

class ApplicationController extends Controller {

	const STATUS_MESSAGE = "App is working";

	public function getStatus(): JsonResponse {
		return $this->resource
			->pushMessage(self::STATUS_MESSAGE)
			->response();
	}

}
