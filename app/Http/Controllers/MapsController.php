<?php

namespace Brewmap\Http\Controllers;

use Brewmap\Models\Map;
use Illuminate\Http\JsonResponse;

class MapsController extends Controller {

	/**
	 * @return JsonResponse
	 */
	public function getAll(): JsonResponse {
		$collection = Map::all();

		return $this->resource
			->collect($collection)
			->response();
	}

}
