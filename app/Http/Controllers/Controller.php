<?php

namespace Brewmap\Http\Controllers;

use Brewmap\Interfaces\IsSystemResource;
use Illuminate\Log\Logger;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Psr\Log\LoggerInterface;

abstract class Controller extends BaseController {

	/**
	 * @var Request
	 */
	protected $request;

	/**
	 * @var IsSystemResource
	 */
	protected $resource;

	/**
	 * @var LoggerInterface
	 */
	protected $logger;

	/**
	 * Controller constructor.
	 * @param Request $request
	 * @param IsSystemResource $resource
	 */
	public function __construct(Request $request, IsSystemResource $resource) {
		$this->request = $request;
		$this->resource = $resource;
		$this->logger = app(Logger::class);
	}

}
