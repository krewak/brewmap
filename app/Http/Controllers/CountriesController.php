<?php

namespace Brewmap\Http\Controllers;

use Brewmap\Models\Country;
use Illuminate\Http\JsonResponse;

class CountriesController extends Controller {

	/**
	 * @return JsonResponse
	 */
	public function getAll(): JsonResponse {
		$collection = Country::all();

		return $this->resource
			->collect($collection)
			->response();
	}

}
