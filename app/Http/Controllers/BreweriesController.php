<?php

namespace Brewmap\Http\Controllers;

use Brewmap\Helpers\Area;
use Brewmap\Models\Brewery;
use Illuminate\Http\JsonResponse;

class BreweriesController extends Controller {

	const STANDARD_COLLECTION_LIMIT = 500;
	const SEARCH_COLLECTION_LIMIT = 10;

	/**
	 * @return JsonResponse
	 */
	public function getAll(): JsonResponse {
		$collection = Brewery::onlyWorking()
			->inRandomOrder()
			->limit(self::STANDARD_COLLECTION_LIMIT)
			->get();

		return $this->resource
			->collect($collection)
			->response();
	}

	/**
	 * @param float ...$coordinates
	 * @return JsonResponse
	 * @throws \Brewmap\Exceptions\InvalidArea
	 */
	public function getAllInLimits(float ...$coordinates): JsonResponse {
		$area = new Area($coordinates);

		$collection = Brewery::onlyWorking()
			->where("latitude", "<=", $area->north())
			->where("longitude", "<=", $area->east())
			->where("latitude", ">=", $area->south())
			->where("longitude", ">=", $area->west())
			->orderByRaw("RAND()")
			->limit(self::STANDARD_COLLECTION_LIMIT)
			->get();

		return $this->resource
			->collect($collection)
			->response();
	}

	/**
	 * @param string $query
	 * @return JsonResponse
	 */
	public function getBySearchPhrase(string $query): JsonResponse {
		$collection = Brewery::query()
			->where("name", "LIKE", "%$query%")
			->inRandomOrder()
			->limit(self::SEARCH_COLLECTION_LIMIT)
			->get();

		return $this->resource
			->collect($collection)
			->response();
	}

}
