<?php

namespace Brewmap\Http\Controllers;

use Brewmap\Exceptions\MapNotFound;
use Brewmap\Models\Map;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

class MapController extends Controller {

	/**
	 * @param string $slug
	 * @return JsonResponse
	 * @throws MapNotFound
	 */
	public function getBySlug(string $slug): JsonResponse {
		try {
			$map = Map::query()
				->where("slug", $slug)
				->firstOrFail();
		} catch(ModelNotFoundException $exception) {
			throw new MapNotFound();
		}

		return $this->resource
			->resource($map)
			->response();
	}

}
