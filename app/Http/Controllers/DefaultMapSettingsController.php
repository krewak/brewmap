<?php

namespace Brewmap\Http\Controllers;

use Brewmap\Helpers\Coordinates;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

class DefaultMapSettingsController extends Controller {

	/**
	 * @return JsonResponse
	 * @throws \Brewmap\Exceptions\InvalidCoordinateValue
	 */
	public function getDefaultBounds(): JsonResponse {
		$coordinates = new Collection([
			(new Coordinates(54.833333, 18.066667))->get(),
			(new Coordinates(49.002380, 22.847100))->get(),
			(new Coordinates(50.868520, 24.145850))->get(),
			(new Coordinates(52.838270, 14.122980))->get(),
		]);

		return $this->resource
			->resource($coordinates)
			->response();
	}

}
