<?php

namespace Brewmap\Resources;

use Brewmap\Interfaces\IsSystemResource;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Request;

/**
 * Class IsSystemResource
 * @package Brewmap\Resources
 * @method JsonResponse response(Request $request = null)
 */
class SystemResource extends JsonResource implements IsSystemResource {

	protected $messages = [];

	public function resource(Arrayable $resource): IsSystemResource {
		$this->resource = $resource;
		return $this;
	}

	public function pushMessage(string $message): IsSystemResource {
		$this->messages[] = $message;
		return $this;
	}

	public function with($request) {
		return [
			"messages" => $this->messages,
		];
	}

	public function collect(Arrayable $resource): AnonymousResourceCollection	{
		$collection = self::collection($resource);
		$collection->additional($this->with(null));
		return $collection;
	}

}
