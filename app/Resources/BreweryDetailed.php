<?php

namespace Brewmap\Resources;

use Brewmap\Models\Brewery as BreweryModel;
use Illuminate\Http\Request;

class BreweryDetailed extends Brewery {

	/**
	 * @param Request $request
	 * @return array
	 */
	public function toArray($request): array {
		/** @var BreweryModel $brewery */
		$brewery = $this;

		return [
			"slug" => $brewery->slug,
			"name" => $brewery->name,
			"altName" => $brewery->alt_name,
			"status" => new BreweryStatus($brewery->status),
			"description" => "Lorem ipsum...",
			"location" => [
				"coordinates" => $brewery->coordinates->get(),
				"country" => new Country($brewery->country),
				"city" => $brewery->city,
				"address" => $brewery->address,
			],
			"details" => [
				"open" => false,
				"tap" => true,
				"tours" => true,
			],
			"online" => [
				"facebook" => null,
				"ontap" => null,
				"ratebeer" => null,
				"untappd" => null,
			]
		];
	}

}
