<?php

namespace Brewmap\Resources;

use Brewmap\Models\BreweryStatus as BreweryStatusModel;
use Illuminate\Http\Request;

class BreweryStatus extends SystemResource {

	/**
	 * @param Request $request
	 * @return array
	 */
	public function toArray($request): array {
		/** @var BreweryStatusModel $status */
		$status = $this;

		return [
			"id" => $status->id,
			"name" => $status->name,
		];
	}

}
