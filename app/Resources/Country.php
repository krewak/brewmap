<?php

namespace Brewmap\Resources;

use Brewmap\Models\Country as CountryModel;
use Illuminate\Http\Request;

class Country extends SystemResource {

	/**
	 * @param Request $request
	 * @return array
	 */
	public function toArray($request): array {
		/** @var CountryModel $country */
		$country = $this;

		return [
			"slug" => $country->slug,
			"name" => $country->name,
			"symbol" => $country->symbol,
		];
	}

}
