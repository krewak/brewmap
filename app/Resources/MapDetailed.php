<?php

namespace Brewmap\Resources;

use Brewmap\Models\Map as MapModel;
use Illuminate\Http\Request;

class MapDetailed extends SystemResource {

	/**
	 * @param Request $request
	 * @return array
	 */
	public function toArray($request): array {
		/** @var MapModel $map */
		$map = $this;
		$breweries = new Brewery(null);
		$breweries = $breweries->collect($map->breweries);

		return [
			"slug" => $map->slug,
			"name" => $map->name,
			"breweries" => $breweries
		];
	}

}
