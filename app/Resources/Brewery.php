<?php

namespace Brewmap\Resources;

use Brewmap\Models\Brewery as BreweryModel;
use Illuminate\Http\Request;

class Brewery extends SystemResource {

	/**
	 * @param Request $request
	 * @return array
	 */
	public function toArray($request): array {
		/** @var BreweryModel $brewery */
		$brewery = $this;

		return [
			"slug" => $brewery->slug,
			"name" => $brewery->name,
			"altName" => $brewery->name,
			"searchName" => $brewery->search_name,
			"coordinates" => $brewery->coordinates->get(),
			"country" => new Country($brewery->country),
			"status" => new BreweryStatus($brewery->status),
		];
	}

	public function with($request) {
		$with = parent::with($request);
		$with["count"] = BreweryModel::count();
		return $with;
	}

}
