<?php

require_once __DIR__ . "/../vendor/autoload.php";

use Brewmap\Console\Kernel;
use Brewmap\Exceptions\Handler;
use Brewmap\Http\Middleware\Authenticated;
use Brewmap\Http\Middleware\Guest;
use Brewmap\Http\Middleware\HandleToken;
use Brewmap\Http\Middleware\RefreshToken;
use Brewmap\Providers\ApplicationServiceProvider;
use Brewmap\Providers\DevelopmentEnvironmentServiceProvider;
use Brewmap\Providers\ObserversServiceProvider;
use Brewmap\Providers\ResourceInjectionServiceProvider;
use Brewmap\Providers\RouterServiceProvider;
use Dotenv\Dotenv;
use Dotenv\Exception\InvalidPathException;
use Illuminate\Contracts\Console\Kernel as KernelContract;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Laravel\Lumen\Application;

try {
	$env = new Dotenv(__DIR__ . "/../");
	$env->load();
} catch(InvalidPathException $e) {
	//
}

$app = new Application(realpath(__DIR__ . "/../"));

$app->withEloquent();

$app->singleton(ExceptionHandler::class, Handler::class);
$app->singleton(KernelContract::class, Kernel::class);

$app->routeMiddleware([
	"auth" => Authenticated::class,
	"guest" => Guest::class,
	"token" => HandleToken::class,
	"refresh" => RefreshToken::class,
]);

$app->register(ApplicationServiceProvider::class);
$app->register(ObserversServiceProvider::class);
$app->register(ResourceInjectionServiceProvider::class);
$app->register(RouterServiceProvider::class);

if(app()->environment("local")) {
	$app->register(DevelopmentEnvironmentServiceProvider::class);
}

return $app;
