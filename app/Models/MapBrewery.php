<?php

namespace Brewmap\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class MapBrewery
 * @package Brewmap\Models
 *
 * @property string $map_uuid
 * @property Map $map
 *
 * @property string $brewery_uuid
 * @property Brewery $brewery
 */
class MapBrewery extends Model {

	protected $table = "map_breweries";
	protected $primaryKey = null;
	public $incrementing = false;

	public function map(): BelongsTo {
		return $this->belongsTo(Map::class);
	}

	public function brewery(): BelongsTo {
		return $this->belongsTo(Brewery::class);
	}

}
