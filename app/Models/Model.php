<?php

namespace Brewmap\Models;

use Brewmap\Interfaces\IsSystemModel;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Support\Collection;

/**
 * Class Model
 * @package Brewmap\Models
 * @method static self firstOrCreate(array $parameters)
 * @method static findOrFail(array $parameters)
 * @method static where(string $field, string $operator, string $value = "")
 * @method static create(array $builder)
 * @method static select(string $select)
 * @method static int count()
 * @method static Collection pluck(string|array $columns)
 */
abstract class Model extends BaseModel implements IsSystemModel {

}
