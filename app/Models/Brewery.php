<?php

namespace Brewmap\Models;

use Brewmap\Helpers\Coordinates;
use Brewmap\Scopes\AcceptedBreweryDraftsScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Brewery
 * @package Brewmap\Models
 * @property string $uuid
 * @property string $slug
 *
 * @property string $name
 * @property string $alt_name
 * @property string $search_name
 * @property string $full_name
 *
 * @property string $country_uuid
 * @property Country $country
 *
 * @property string $status_id
 * @property BreweryStatus $status
 *
 * @property Coordinates $coordinates
 * @property double $latitude
 * @property double $longitude
 * @property string $city
 * @property string $address
 *
 * @method static Builder onlyWorking()
 */
class Brewery extends Model {

	protected $table = "breweries";
	protected $hidden = ["uuid", "created_at", "updated_at"];
	protected $primaryKey = "uuid";
	protected $keyType = "string";
	public $incrementing = false;

	protected static function boot() {
		parent::boot();
		static::addGlobalScope(new AcceptedBreweryDraftsScope());
	}

	public static function find(string $slug) {
		return self::query()->where("slug", $slug)->first();
	}

	public function country(): BelongsTo {
		return $this->belongsTo(Country::class);
	}

	public function status(): BelongsTo {
		return $this->belongsTo(BreweryStatus::class);
	}

	/**
	 * @return Coordinates
	 * @throws \Brewmap\Exceptions\InvalidCoordinateValue
	 */
	public function getCoordinatesAttribute() {
		return new Coordinates($this->latitude, $this->longitude);
	}

	public function scopeOnlyWorking(Builder $query): Builder {
		return $query->whereIn("status_id", BreweryStatus::WORKING_BREWERIES);
	}

}
