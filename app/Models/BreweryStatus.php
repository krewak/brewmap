<?php

namespace Brewmap\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Brewery
 * @package Brewmap\Models
 * @property string $id
 * @property string $name
 */
class BreweryStatus extends Model {

	const BREWERY = "BR";
	const MICROBREWERY = "MB";
	const BREWPUB = "BP";
	const COMPLETED = "CM";
	const CONSTRUCTION = "CN";
	const PLANNED = "PN";
	const CLOSED = "CL";
	const ADAPTED = "AD";
	const RUINED = "RN";
	const DEMOLISHED = "DM";
	const DRAFT = "DR";

	const STATUSES = [
		self::BREWERY => "brewery",
		self::MICROBREWERY => "microbrewery",
		self::BREWPUB => "brewpub",
		self::COMPLETED => "completed",
		self::CONSTRUCTION => "construction",
		self::PLANNED => "planned",
		self::CLOSED => "closed",
		self::ADAPTED => "adapted",
		self::RUINED => "ruined",
		self::DEMOLISHED => "demolished",
		self::DRAFT => "draft",
	];

	const WORKING_BREWERIES = ["BR", "MB", "BP"];
	const PLANNED_BREWERIES = ["CM", "CN", "PN"];
	const CLOSED_BREWERIES = ["CL", "AD", "RN", "DM"];

	protected $table = "brewery_statuses";
	protected $fillable = ["id", "name"];
	protected $keyType = "string";
	public $incrementing = false;

	public static function getDefaultStatus(): string {
		return self::DRAFT;
	}

	public static function isWorking(string $status): bool {
		return in_array($status, self::WORKING_BREWERIES);
	}

	public static function isPlanned(string $status): bool {
		return in_array($status, self::PLANNED_BREWERIES);
	}

	public static function isClosed(string $status): bool {
		return in_array($status, self::CLOSED_BREWERIES);
	}

	public function breweries(): HasMany {
		return $this->hasMany(Brewery::class);
	}

}
