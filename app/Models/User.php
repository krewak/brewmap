<?php

namespace Brewmap\Models;

use Brewmap\Interfaces\CanAuthenticate;

/**
 * Class User
 * @package Brewmap\Models
 * @property string $uuid
 */
class User extends Model implements CanAuthenticate {

	protected $fillable = ["email", "name"];
	protected $hidden = ["uuid", "password"];

	public function getID(): string {
		return (string) $this->uuid;
	}

}
