<?php

namespace Brewmap\Models;

/**
 * Class Country
 * @package Brewmap\Models
 * @property string $uuid
 * @property string $slug
 *
 * @property string $name
 * @property string $symbol
 */
class Country extends Model {

	protected $table = "countries";
	protected $fillable = ["name", "symbol"];
	protected $hidden = ["uuid", "created_at", "updated_at"];
	protected $primaryKey = "uuid";
	protected $keyType = "string";
	public $incrementing = false;

	public static function find(string $slug) {
		return self::query()->where("slug", $slug)->first();
	}

	public static function createEntryForTesting(): self {
		return self::create(["name" => "Sokovia", "symbol" => "so"]);
	}

}
