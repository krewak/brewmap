<?php

namespace Brewmap\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Map
 * @package Brewmap\Models
 * @property string $uuid
 * @property string $slug
 *
 * @property string $name
 * @property string $description
 * @property string $cover_url
 * @property int $breweries_count
 */
class Map extends Model {

	protected $table = "maps";
	protected $fillable = ["name", "description", "cover_url"];
	protected $hidden = ["uuid", "created_at", "updated_at"];
	protected $primaryKey = "uuid";
	protected $keyType = "string";
	public $incrementing = false;

	public static function createEntryForTesting(): self {
		return self::create(["name" => "Map"]);
	}

	public static function find(string $slug) {
		return self::query()->where("slug", $slug)->first();
	}

	public function breweries(): BelongsToMany {
		return $this->belongsToMany(Brewery::class, "map_breweries");
	}

}
