<?php

namespace Brewmap\Exceptions;

use Brewmap\Resources\SystemResource;
use Exception;
use Illuminate\Http\Response;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException as NotFoundHttp;

class Handler extends ExceptionHandler {

	public function render($request, Exception $exception) {
		$resource = new SystemResource(null);

		if($exception instanceof NotFoundHttp) {
			return $resource
				->pushMessage("Page not found.")
				->response()
				->setStatusCode(Response::HTTP_NOT_FOUND);
		}

		if($exception instanceof BreweryNotFound) {
			return $resource
				->pushMessage("Brewery not found.")
				->response()
				->setStatusCode(Response::HTTP_NOT_FOUND);
		}

		if($exception instanceof MapNotFound) {
			return $resource
				->pushMessage("Map not found.")
				->response()
				->setStatusCode(Response::HTTP_NOT_FOUND);
		}

		$resource->pushMessage($exception->getMessage());
		foreach($exception->getTrace() as $trace) {
			if(isset($trace["file"])) {
				$message = $trace["file"] . ":" . $trace["line"] . " @ " . $trace["function"] . "()";
				$resource->pushMessage($message);
			}
		}

		return $resource->response()
			->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
	}

}
