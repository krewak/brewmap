<?php

namespace Brewmap\Providers;

use Brewmap\Models\Brewery;
use Brewmap\Models\Country;
use Brewmap\Models\Map;
use Brewmap\Models\MapBrewery;
use Brewmap\Observers\BreweryObserver;
use Brewmap\Observers\CountryObserver;
use Brewmap\Observers\MapBreweryObserver;
use Brewmap\Observers\MapObserver;
use Illuminate\Support\ServiceProvider;

class ObserversServiceProvider extends ServiceProvider {

	public function boot(): void {
		Brewery::observe(BreweryObserver::class);
		Country::observe(CountryObserver::class);
		Map::observe(MapObserver::class);
		MapBrewery::observe(MapBreweryObserver::class);
	}

}
