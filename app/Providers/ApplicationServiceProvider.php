<?php

namespace Brewmap\Providers;

use Brewmap\Helpers\PolishMonth;
use Faker\Generator as FakerGenerator;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application;

class ApplicationServiceProvider extends ServiceProvider {

	public function register(): void {
		/** @var Application $app */
		$app = $this->app;

		$app->singleton(FakerGenerator::class, function() {
			return FakerFactory::create("pl_PL");
		});
	}

	public function boot(): void {
		Carbon::setLocale("pl");
		Carbon::macro("polishDate", function(): string {
			/** @var \Carbon\Carbon $this */
			$words = [
				$this->format("j"),
				PolishMonth::get($this->format("n")),
				$this->format("Y"),
			];
			return implode(" ", $words);
		});
	}

}
