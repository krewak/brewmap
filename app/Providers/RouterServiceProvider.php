<?php

namespace Brewmap\Providers;

use Brewmap\Http\Routes\AdminRoutes;
use Brewmap\Http\Routes\GuestRoutes;
use Brewmap\Http\Routes\CommonRoutes;
use Brewmap\Interfaces\DispatchesRoutes;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Routing\Router;

class RouterServiceProvider extends ServiceProvider {

	protected $namespace = "Brewmap\Http\Controllers";

	public function boot(): void {
		/** @var Router $router */
		$router = $this->app["router"];

		$router->group([
			"namespace" => $this->namespace,
			"middleware" => "token",
			"prefix" => "api",
		], function(Router $router): DispatchesRoutes {
			return new CommonRoutes($router);
		});

		$router->group([
			"namespace" => $this->namespace,
			"middleware" => "token|guest",
			"prefix" => "api",
		], function(Router $router): DispatchesRoutes {
			return new GuestRoutes($router);
		});

		$router->group([
			"namespace" => $this->namespace,
			"middleware" => "token|auth|refresh",
			"prefix" => "api",
		], function(Router $router): DispatchesRoutes {
			return new AdminRoutes($router);
		});
	}

}
