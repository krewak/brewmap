<?php

namespace Brewmap\Providers;

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Log\Logger;
use Illuminate\Support\ServiceProvider;

class DevelopmentEnvironmentServiceProvider extends ServiceProvider {

	public function register(): void {
		/** @var DatabaseManager $database */
		$database = app(DatabaseManager::class);
		$database->listen(function(QueryExecuted $query) {
			app(Logger::class)->channel("queries")->info($query->sql);
		});
	}

}
