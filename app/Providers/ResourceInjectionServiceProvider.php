<?php

namespace Brewmap\Providers;

use Brewmap\Http\Controllers\BreweriesController;
use Brewmap\Http\Controllers\BreweryController;
use Brewmap\Http\Controllers\CountriesController;
use Brewmap\Interfaces\IsSystemResource;
use Brewmap\Resources\Brewery;
use Brewmap\Resources\Country;
use Brewmap\Resources\BreweryDetailed;
use Brewmap\Resources\SystemResource;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application;

class ResourceInjectionServiceProvider extends ServiceProvider {

	/** @var Application $app */
	protected $app;

	public function register(): void {
		$this->app->bind(IsSystemResource::class, function() {
			return new SystemResource(null);
		});

		$this->inject(new BreweryDetailed(null), [BreweryController::class]);
		$this->inject(new Brewery(null), [BreweriesController::class]);
		$this->inject(new Country(null), [CountriesController::class]);
	}

	public function inject(IsSystemResource $resource, array $controllers) {
		$this->app->when($controllers)
			->needs(IsSystemResource::class)
			->give(function() use($resource) { return $resource; });
	}

}
