<?php

namespace Brewmap\Observers;

use Brewmap\Helpers\ModelSlugger;
use Brewmap\Models\Map;
use Faker\Provider\Uuid;

class MapObserver {

	use ModelSlugger;

	public function creating(Map $map): void {
		$map->uuid = Uuid::uuid();
		$map->slug = $this->buildUniqueSlug(new Map(), str_slug($map->name));
	}

}
