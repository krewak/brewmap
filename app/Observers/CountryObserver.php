<?php

namespace Brewmap\Observers;

use Brewmap\Helpers\ModelSlugger;
use Brewmap\Models\Country;
use Faker\Provider\Uuid;

class CountryObserver {

	use ModelSlugger;

	public function creating(Country $country): void {
		$country->uuid = Uuid::uuid();
		$country->slug = $this->buildUniqueSlug(new Country(), str_slug($country->name));
	}

}