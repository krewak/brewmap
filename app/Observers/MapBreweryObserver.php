<?php

namespace Brewmap\Observers;

use Brewmap\Models\MapBrewery;

class MapBreweryObserver {

	public function creating(MapBrewery $pivot): void {
		$pivot->map->breweries_count++;
		$pivot->map->save();
	}

}
