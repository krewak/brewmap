<?php

namespace Brewmap\Observers;

use Brewmap\Helpers\ModelSlugger;
use Brewmap\Models\Brewery;
use Faker\Provider\Uuid;

class BreweryObserver {

	use ModelSlugger;

	public function creating(Brewery $brewery): void {
		$brewery->uuid = Uuid::uuid();
		$brewery->slug = $this->buildUniqueSlug(new Brewery, str_slug($brewery->name));
	}

}