<?php

namespace Brewmap\Helpers;

use Brewmap\Exceptions\InvalidCoordinateValue;

/**
 * Class Coordinates
 * @package Brewmap\Helpers
 */
class Coordinates {

	const MANTISSA = 6;

	private $latitude;
	private $longitude;

	/**
	 * Coordinates constructor.
	 * @param float $latitude
	 * @param float $longitude
	 * @throws InvalidCoordinateValue
	 */
	public function __construct(float $latitude, float $longitude) {
		if(abs($latitude) > 90) {
			throw new InvalidCoordinateValue("Invalid value. Latitude cannot be larger than ±90deg.");
		}

		if(abs($longitude) > 180) {
			throw new InvalidCoordinateValue("Invalid value. Longitude cannot be larger than ±180deg.");
		}

		$this->latitude = $latitude;
		$this->longitude = $longitude;
	}

	public function get(): array {
		return [
			"latitude" => $this->getLatitude(),
			"longitude" => $this->getLongitude(),
		];
	}

	public function getLatitude(): string {
		return $this->getFormattedCoordinate($this->latitude);
	}

	public function getLongitude(): string {
		return $this->getFormattedCoordinate($this->longitude);
	}

	private function getFormattedCoordinate(float $value): string {
		return number_format($value, self::MANTISSA);
	}

}
