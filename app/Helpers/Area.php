<?php

namespace Brewmap\Helpers;

use Brewmap\Exceptions\InvalidArea;

/**
 * Class Area
 * @package Brewmap\Helpers
 */
class Area {

	private $north;
	private $east;
	private $south;
	private $west;


	/**
	 * Area constructor.
	 * @param array $limits
	 * @throws InvalidArea
	 */
	public function __construct(array $limits) {
		$this->buildLimits($limits);

		if(abs($this->north) >= 90 || abs($this->south) >= 90) {
			throw new InvalidArea("Invalid coordinates. Latitude cannot be larger than 90deg.");
		}

		if($this->east >= 180) { $this->east = 180; }
		if($this->west <= -180) { $this->west = -180; }
	}

	public function north(): string {
		return number_format($this->north, Coordinates::MANTISSA);
	}

	public function south(): string {
		return number_format($this->south, Coordinates::MANTISSA);
	}

	public function west(): string {
		return number_format($this->west, Coordinates::MANTISSA);
	}

	public function east(): string {
		return number_format($this->east, Coordinates::MANTISSA);
	}

	private function buildLimits(array $limits): void {
		$this->north = $limits[0];
		$this->east = $limits[1];
		$this->south = $limits[2];
		$this->west = $limits[3];
	}

}
