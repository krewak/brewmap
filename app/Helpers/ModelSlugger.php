<?php

namespace Brewmap\Helpers;

use Brewmap\Interfaces\IsSystemModel;
use Illuminate\Support\Collection;

trait ModelSlugger {

	private function buildUniqueSlug(IsSystemModel $model, string $slug): string {
		/** @var Collection $collection */
		$collection = $model::query()
			->withoutGlobalScopes()
			->select("slug")
			->where("slug", "LIKE", "$slug%")
			->pluck("slug")
			->filter(function(string $currentSlug) use($slug): bool {
				if($currentSlug === $slug) {
					return true;
				}

				$slugRemnants = explode($slug . "-", $currentSlug);
				$slugRemnants = array_values(array_filter($slugRemnants));

				return is_numeric($slugRemnants[0]);
			});

		$count = $collection->count();

		if($count) {
			$count++;
			$slug = str_slug("$slug $count");
		}

		return $slug;
	}

}
