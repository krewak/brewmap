<?php

namespace Brewmap\Scopes;

use Brewmap\Models\BreweryStatus;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class AcceptedBreweryDraftsScope implements Scope {

	public function apply(Builder $builder, Model $model) {
		$builder->where("status_id", "<>", BreweryStatus::DRAFT);
	}
}
