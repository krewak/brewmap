<?php

namespace Brewmap\Interfaces;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Interface IsSystemResource
 * @package Brewmap\Interfaces
 * @property mixed $resource
 * @method JsonResponse response(Request $request = null)
 */
interface IsSystemResource {

	public function pushMessage(string $message): self;
	public function resource(Arrayable $resource): self;
	public function collect(Arrayable $collection): AnonymousResourceCollection;

}
