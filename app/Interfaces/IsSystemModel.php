<?php

namespace Brewmap\Interfaces;

use Illuminate\Database\Eloquent\Builder;

/**
 * Interface IsSystemModel
 *
 * @method static Builder query()
 */
interface IsSystemModel {

}
