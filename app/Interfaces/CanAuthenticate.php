<?php

namespace Brewmap\Interfaces;

interface CanAuthenticate {

	public function getID(): string;

}
